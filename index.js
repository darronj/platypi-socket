var connect = require('connect'),
    http = require('http'),
    serveStatic = require('serve-static'),
    morgan = require('morgan'),
    socketio = require('socket.io');

var app = connect();

app.use(morgan('combined'));
app.use(serveStatic('public/', { 'index': ['index.html'] }));

// return the server variable, as we are going to need it now.
var server = http.createServer(app).listen(3000);
console.log('listening on port 3000');

// init socket.io server
var io = socketio.listen(server);
console.log('sockets live, awaiting connections');
io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect', function() {
        console.log('a user disconnected');
    });   
});

// some bogus data to send messages to connected sockets
var i = 1;
setInterval(function() {
    io.emit('orderCount', i);
    i++;
}, 3000); //sends update every 3 seconds.



