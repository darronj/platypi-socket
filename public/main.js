/// <reference path="_references.d.ts" />
require('platypus');
require('platypusui');
require('./polyfills');
require('./scripts');
require('./app/app');
require('./viewcontrols/survey/survey.viewcontrol');
require('./viewcontrols/clinic/clinic.viewcontrol');
require('./viewcontrols/followup/followup.viewcontrol');
require('./viewcontrols/login/login.vc');
require('./common/controls/navbar/navbar.templatecontrol');
//# sourceMappingURL=main.js.map