/// <reference path="../../_references.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var plat = require('platypus');
var BaseViewControl = require('../base/base.viewcontrol');
var HomeViewControl = require('../home/home.viewcontrol');
var SurveyViewControl = (function (_super) {
    __extends(SurveyViewControl, _super);
    function SurveyViewControl() {
        _super.apply(this, arguments);
        this.templateString = require('./survey.viewcontrol.html');
        this.context = {
            rating: {},
            comments: {}
        };
    }
    SurveyViewControl.prototype.initialize = function () {
        this.context.rating = 7;
        this.context.comments = '';
    };
    SurveyViewControl.prototype.setRating = function (val) {
        console.log(val);
        this.context.rating = val;
    };
    SurveyViewControl.prototype.save = function () {
        console.log('completed survey');
        console.log('================');
        console.log('rating: ' + this.context.rating);
        console.log('comments:\n' + this.context.comments);
        console.log('================');
        this.navigator.navigate(HomeViewControl);
    };
    SurveyViewControl.prototype.loaded = function () { };
    SurveyViewControl.prototype.canNavigateTo = function (parameters, query) { };
    SurveyViewControl.prototype.canNavigateFrom = function () { };
    SurveyViewControl.prototype.navigatedTo = function (parameters, query) { };
    SurveyViewControl.prototype.navigatingFrom = function () { };
    SurveyViewControl.prototype.dispose = function () { };
    return SurveyViewControl;
})(BaseViewControl);
plat.register.viewControl('survey', SurveyViewControl);
module.exports = SurveyViewControl;
//# sourceMappingURL=survey.viewcontrol.js.map