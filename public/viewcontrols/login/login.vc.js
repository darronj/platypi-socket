/// <reference path="../../_references.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var plat = require('platypus');
var BaseViewControl = require('../base/base.viewcontrol');
var LoginViewControl = (function (_super) {
    __extends(LoginViewControl, _super);
    function LoginViewControl() {
        _super.apply(this, arguments);
        this.templateString = require('./login.vc.html');
        this.context = {};
    }
    return LoginViewControl;
})(BaseViewControl);
plat.register.viewControl('login', LoginViewControl);
module.exports = LoginViewControl;
//# sourceMappingURL=login.vc.js.map