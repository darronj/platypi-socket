/// <reference path="../../_references.d.ts" />

import plat = require('platypus');
import BaseViewControl = require('../base/base.viewcontrol');

class LoginViewControl extends BaseViewControl {
    templateString: string = require('./login.vc.html');

    context: any = {};
}


plat.register.viewControl('login', LoginViewControl);

export = LoginViewControl;
