/// <reference path="../../_references.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var plat = require('platypus');
var BaseViewControl = require('../base/base.viewcontrol');
var ClinicViewControl = (function (_super) {
    __extends(ClinicViewControl, _super);
    function ClinicViewControl() {
        _super.apply(this, arguments);
        this.templateString = require('./clinic.viewcontrol.html');
        this.context = { clinic: 'my clinic in albertville' };
    }
    ClinicViewControl.prototype.initialize = function () {
        this.context.clinic = 'Cullman Urgent Care';
    };
    ClinicViewControl.prototype.loaded = function () { };
    ClinicViewControl.prototype.canNavigateTo = function (parameters, query) { };
    ClinicViewControl.prototype.canNavigateFrom = function () { };
    ClinicViewControl.prototype.navigatedTo = function (parameters, query) { };
    ClinicViewControl.prototype.navigatingFrom = function () { };
    ClinicViewControl.prototype.dispose = function () { };
    return ClinicViewControl;
})(BaseViewControl);
plat.register.viewControl('clinic', ClinicViewControl);
module.exports = ClinicViewControl;
//# sourceMappingURL=clinic.viewcontrol.js.map