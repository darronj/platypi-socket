/// <reference path="../../_references.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var plat = require('platypus');
var BaseViewControl = require('../base/base.viewcontrol');
var HomeViewControl = (function (_super) {
    __extends(HomeViewControl, _super);
    function HomeViewControl() {
        _super.call(this);
        this.templateString = require('./home.viewcontrol.html');
        this.context = {
            count: 'Awaiting Data'
        };
        this._socket = window.io();
    }
    HomeViewControl.prototype.loaded = function () {
        var _this = this;
        this._socket.on('orderCount', function (data) {
            _this.context.count = 'Todays current orders: ' + data;
        });
    };
    return HomeViewControl;
})(BaseViewControl);
plat.register.viewControl('home', HomeViewControl);
module.exports = HomeViewControl;
//# sourceMappingURL=home.viewcontrol.js.map