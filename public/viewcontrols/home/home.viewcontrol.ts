﻿/// <reference path="../../_references.d.ts" />

import plat = require('platypus');
import BaseViewControl = require('../base/base.viewcontrol');

interface IOrderCount {
    count: string;
}

interface Window {
    io(): SocketIOClient.Socket;
}
declare var window: Window;

class HomeViewControl extends BaseViewControl {
    templateString: string = require('./home.viewcontrol.html');
    protected _socket: SocketIOClient.Socket;
    
    constructor() {
        super();
        this._socket = window.io();
    }
    
    loaded() {
        this._socket.on('orderCount', 
            (data:any) => {
                this.context.count = 'Todays current orders: ' + data;
            }
        );
    }

    context: IOrderCount = {
        count: 'Awaiting Data'
    };
}

plat.register.viewControl('home', HomeViewControl);

export = HomeViewControl;
