/// <reference path="../../_references.d.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var plat = require('platypus');
var BaseViewControl = require('../base/base.viewcontrol');
var FollowupViewControl = (function (_super) {
    __extends(FollowupViewControl, _super);
    function FollowupViewControl() {
        _super.apply(this, arguments);
        this.templateString = require('./followup.viewcontrol.html');
        this.context = {};
    }
    FollowupViewControl.prototype.initialize = function () { };
    FollowupViewControl.prototype.loaded = function () { };
    FollowupViewControl.prototype.canNavigateTo = function (parameters, query) {
        if (this.navigator.uid != '0') {
            this.navigator.navigate('login');
            return false;
        }
    };
    FollowupViewControl.prototype.canNavigateFrom = function () { };
    FollowupViewControl.prototype.navigatedTo = function (parameters, query) { };
    FollowupViewControl.prototype.navigatingFrom = function () { };
    FollowupViewControl.prototype.dispose = function () { };
    return FollowupViewControl;
})(BaseViewControl);
plat.register.viewControl('followup', FollowupViewControl);
module.exports = FollowupViewControl;
//# sourceMappingURL=followup.viewcontrol.js.map